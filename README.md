# Code Behind jonatanl.me Portfolio Website
This repository contains the code for the portfolio site at jonatanl.me.

## Technologies Used in This Project
- [React](https://reactjs.org/)
- [Gatsby](https://www.gatsbyjs.org/)
- [Gitlab](https://gitlab.com/)
- [npm](https://nodejs.org/en/)

## Prerequisites
1. Create an account at [Formik](https://formik.com/?utm_source=smakosh) and grab your form endpoint url. This is used to send the response from the contact form
2. Grab a Google recaptcha key from [Google Recaptcha](https://www.google.com/recaptcha/admin)
3. Create [Google Analytics](http://analytics.google.com) project for your website

## Setup
1. `git clone git@gitlab.com:devguides/jonatanl.me.git` - Clone the project
2. `npm install` - Install dependencies
3. `cp .env.example .env` - Copy a the variable example file and fill it out with your own variables
4. `npm run dev` - Run the development environment

## Structure

```bash
.                 
├── src
│   └── assets              # Assets
│   │   │── icons             # icons
│   │   │── illustrations     # illustrations from (undraw.co)
│   │   └── thumbnail         # cover of your website when it's shared to social media
│   ├── components          # Components
│   │   │── common            # Common components
│   │   │── landing           # Components used on the landing page
│   │   └── theme             # Header & Footer
│   │── content             # Contains Markdown files for the longer content texts
│   │── content             # Contains configuration and data files
│   │── pages               # Gatsby pages
│   └── util                # Utility functions
└── static                  # Favicon and social icons
```

## `npm` Scripts
- `dev` - Run development environment locally
- `build` - Build the project for production
- `lint` - Run lint
- `lint:fix` - Auto fix lint errors
- `clean` - Clean cache files in the project

## Refrences
This portfolio site is based off the work by [Smakosh](https://smakosh.com). The theme can be found at [Gatsby-theme-portfolio](https://github.com/smakosh/gatsby-theme-portfolio).
