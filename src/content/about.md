---
section: "about"
---

# About
My name is Jonatan Lund and I work as a full-stack developer.
I do everything from implementing React frontends, to implement the backend API's and setting up DevOps pipelines.
