import WorkData from './work'
import SkillsData from './skills'
import Config from './config'
import NavbarItems from './navbar'

export { WorkData, SkillsData, Config, NavbarItems }
