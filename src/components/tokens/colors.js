export default {
  primary: '#6c63ff',
  primaryText: 'rgba(0,0,0,0.8)',
  primaryOverlay: 'efeeff',
  buttonColor: '#0074d9',
  buttonColorSecondary: '#001F3F'
}
