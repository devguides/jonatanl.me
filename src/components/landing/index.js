import Intro from './Intro'
import About from './About'
import Contact from './Contact'
import Skills from './Skills'
import Work from './Work'
import Education from './Education'
import Blog from './Blog'

export { Intro, About, Contact, Skills, Work, Education, Blog }
