const getTimeframe = (from, to) => {
  if (to) return `${from} - ${to}`
  return `${from}`
}

export default {
  getTimeframe
}
