import React from 'react'
import { Layout, SEO } from 'components/common'
import { Intro, Skills, Contact, About, Work, Education, Blog } from 'components/landing'

export default () => (
  <Layout>
    <SEO />
    <Intro />
    <About />
    <Skills />
    <Blog />
    <Work />
    <Education />
    <Contact />
  </Layout>
)
